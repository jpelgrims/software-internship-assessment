process.stdin.on('data', (data) => {
  if (validate_number(data.toString())) {
    process.exit(0);
  }
  process.exit(42);
});


function validate_number(data: string) : boolean {
  let line: string = data.replace(/\s/g, ""); // Remove whitespace

  // Perform basic validations, i.e. more than one value, and all values are numbers
  if (line.length <= 1 || !line.match(/^[0-9]+$/)) {
    return false;
  }

  let checksum: number = line.split('') // Turn into array of chars
                              .map((val) => Number(val)) // Turn into array of numbers
                              .reverse().map((val, i) => i % 2 == 0 ? val : val*2) // Double every second value, starting from the right
                              .map((val) => val > 9 ? val-9 : val) // Subtract 9 from all numbers larger than 9
                              .reduce((a, b) => a + b, 0); // Sum all values
  return checksum % 10 == 0;
}